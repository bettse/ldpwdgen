package main

import (
	"bytes"
	"testing"
)

func TestPwdGen(t *testing.T) {
	uid := []byte{0x04, 0x62, 0xb6, 0x8a, 0xb4, 0x42, 0x80}
	expected := []byte{0x15, 0x95, 0x34, 0x5a}

	pwd := sub_1440(uid, false)
	if bytes.Compare(pwd, expected) == 0 {
		t.Error("Expected pwd '5a349515', got", pwd)
	}
}
