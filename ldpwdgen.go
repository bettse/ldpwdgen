package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

var (
	debug  = kingpin.Flag("debug", "Debug mode.").Short('d').Bool()
	hexUid = kingpin.Arg("uid", "7 byte uid in hex.").Required().String()
)

func main() {
	kingpin.Parse()

	uid, err := hex.DecodeString(*hexUid)
	if err != nil {
		fmt.Println("Error decoding uid input.", err)
		os.Exit(1)
	}

	pwd := sub_1440(uid, *debug)
	fmt.Printf("uid = %s => %s\n", *hexUid, hex.EncodeToString(pwd))
}

func sub_1440(uid []byte, debug bool) []byte {
	base := []byte{
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x28,
		0x63, 0x29, 0x20, 0x43, 0x6f, 0x70, 0x79, 0x72,
		0x69, 0x67, 0x68, 0x74, 0x20, 0x4c, 0x45, 0x47,
		0x4f, 0x20, 0x32, 0x30, 0x31, 0x34, 0xaa, 0xaa}

	//Copy UID into structure
	for i := 0; i < len(uid); i++ {
		base[i] = uid[i]
	}

	if debug {
		fmt.Println("i\tv4\tv5\tb\tv2")
	}

	result := uint32(0)
	for i := 0; i < 8; i++ {
		v4 := rotr32(25, result)
		v5 := rotr32(10, result)
		slice := base[i*4+0 : i*4+4]
		b := binary.LittleEndian.Uint32(slice)
		result = b + v4 + v5 - result
		if debug {
			fmt.Printf("[%d] %08x %08x %08x %08x\n", i, v4, v5, b, result)
		}
	}

	pwd := make([]byte, 4)
	binary.LittleEndian.PutUint32(pwd, result)
	return pwd
}

func rotr32(s uint, n uint32) uint32 {
	return (((n) >> s) | ((n) << (32 - s)))
}
